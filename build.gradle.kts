import org.gradle.internal.impldep.org.apache.maven.model.Profile

plugins {
    id("java")
    id("war")
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

sourceSets {
    main {
        java {
            srcDir("src/main/java")
        }
        resources {
            srcDir("src/main/resources")
        }
    }
}

war {
    base.archivesName = "calculator-api"
}

ext {
    set("jUnitVersion", "5.10.2")
    set("iUnitMockitoVersion", "5.11.0")
    set("servletVersion", "4.0.1")
}


dependencies {
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    compileOnly("jakarta.servlet:jakarta.servlet-api:6.1.0-M2")
    implementation("jakarta.servlet:jakarta.servlet-api:6.1.0-M2")
}

tasks.test {
    useJUnitPlatform()
}