package org.example.ServiceImplementations;

import org.example.Helpers.ArithmeticHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class CoreCalculatorTest {

    private CoreCalculator service;

    @BeforeEach
    void init() {
        service = new CoreCalculator(new ArithmeticHelper());
    }

    @Test
    void add_fail() {
        assertThrows(RuntimeException.class, () -> {
           service.add(Integer.MAX_VALUE, Integer.MAX_VALUE);
        });

        assertThrows(RuntimeException.class, () -> {
            service.add(Integer.MIN_VALUE, Integer.MIN_VALUE);
        });
    }

    @Test
    void add_success() {
        Assertions.assertDoesNotThrow(() -> {
            service.add(5, 1);
        });
    }

    @Test
    void addMany_fail() {
        assertThrows(IllegalArgumentException.class, () -> {
            service.addMany(null);
        });

        assertThrows(RuntimeException.class, () -> {
           service.addMany(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
        });

        assertThrows(RuntimeException.class, () -> {
            service.addMany(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
        });
    }

    @Test
    void addMany_success() {
        assertDoesNotThrow(() -> {
            service.addMany(1, 2, 3, 4);
        });
    }

    @Test
    void subtract_fail() {
        assertThrows(RuntimeException.class, () -> {
           service.subtract(Integer.MIN_VALUE, Integer.MAX_VALUE);
        });

        assertThrows(RuntimeException.class, () -> {
            service.subtract(Integer.MAX_VALUE, Integer.MIN_VALUE);
        });
    }

    @Test
    void subtract_success() {
        assertDoesNotThrow(() -> {
            service.subtract(0, 1);
        });
    }

    @Test
    void subtractMany_fail() {
        assertThrows(IllegalArgumentException.class, () -> {
            service.subtractManyFrom(0, null);
        });

        assertThrows(RuntimeException.class, () -> {
            service.subtractManyFrom(0, Integer.MAX_VALUE, Integer.MAX_VALUE);
        });
    }

    @Test
    void subtractMany_success() {
        assertDoesNotThrow(() -> {
            service.subtractManyFrom(0, 1, 2);
        });
    }

    @Test
    void multiply_fail() {
        assertThrows(RuntimeException.class, () -> {
            service.multiply(Integer.MAX_VALUE, Integer.MAX_VALUE);
        });

        assertThrows(RuntimeException.class, () -> {
            service.multiply(Integer.MIN_VALUE, Integer.MIN_VALUE);
        });
    }

    @Test
    void multiply_success() {
        assertDoesNotThrow(() -> {
            service.multiply(1, 2);
        });
    }

    @Test
    void multiplyMany_fail() {
        assertThrows(IllegalArgumentException.class, () -> {
            service.multiplyMany(null);
        });

        assertThrows(RuntimeException.class, () -> {
            service.multiplyMany(2, 3, Integer.MAX_VALUE);
        });
    }

    @Test
    void multiplyMany_success() {
        assertDoesNotThrow(() -> {
            service.multiplyMany(7, 2, 4);
        });
    }

    @Test
    void divide_fail() {
        assertThrows(RuntimeException.class, () -> {
           service.divide(Float.MIN_VALUE, 2);
        });

        assertThrows(RuntimeException.class, () -> {
            service.divide(Float.MAX_VALUE, 0.5f);
        });
    }

    @Test
    void divide_success() {
        assertDoesNotThrow(() -> {
            service.divide(1, 2);
            service.divide(10, 5);
        });

        assertEquals(2.5, service.divide(10, 4));
    }

    @Test
    void divideMany_fail() {
        assertThrows(RuntimeException.class, () -> {
            service.divideManyFrom(1, 2, Integer.MAX_VALUE);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            service.divideManyFrom(1, null);
        });
    }

    @Test
    void divideMany_success() {
        assertDoesNotThrow(() -> {
            service.divideManyFrom(1, 2, 3);
        });

        assertEquals(1, service.divideManyFrom(10, 5, 2));
    }
}
