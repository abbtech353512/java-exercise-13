package org.example.Services;

import org.example.Helpers.ArithmeticHelper;

import java.util.Arrays;

public interface CalculatorService {
    int add(int left, int right);
    int addMany(int... numbers);

    int subtract(int left, int right);
    int subtractManyFrom(int left, int... subtractingNums);

    int multiply(int left, int right);
    int multiplyMany(int... numbers);

    float divide(float left, float right);
    float divideManyFrom(int left, int... dividingNums);
}