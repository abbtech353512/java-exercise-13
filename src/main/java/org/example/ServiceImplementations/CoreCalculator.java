package org.example.ServiceImplementations;

import org.example.Helpers.ArithmeticHelper;
import org.example.Services.CalculatorService;

import java.util.Arrays;

public class CoreCalculator implements CalculatorService {

    private ArithmeticHelper _helper;

    public CoreCalculator(ArithmeticHelper helper) {
        _helper = helper;
    }

    @Override
    public int add(int left, int right) {
        long result = (long) left + right;

        try {
            _helper.checkIfValid(result);
        }
        catch (ArithmeticException e) {
            throw new RuntimeException(e);
        }

        return (int) result;
    }

    @Override
    public int addMany(int... numbers) {
        if (numbers == null) {
            throw new IllegalArgumentException("Parameter:[numbers] cannot be null.");
        }

        return Arrays.stream(numbers).reduce(0, this::add);
    }

    @Override
    public int subtract(int left, int right) {
        long result = (long) left - right;

        try {
            _helper.checkIfValid(result);
        }
        catch (ArithmeticException e) {
            throw new RuntimeException(e);
        }

        return (int) result;
    }

    @Override
    public int subtractManyFrom(int left, int... subtractingNums) {
        if (subtractingNums == null) {
            throw new IllegalArgumentException("Parameter:[subtractingNums] cannot be null.");
        }

        int subtractingNumberResult = addMany(subtractingNums);
        return subtract(left, subtractingNumberResult);
    }

    @Override
    public int multiply(int left, int right) {
        long result = (long) left * right;

        try {
            _helper.checkIfValid(result);
        }
        catch (ArithmeticException e) {
            throw new RuntimeException(e);
        }

        return (int) result;
    }

    @Override
    public int multiplyMany(int... numbers) {
        if (numbers == null) {
            throw new IllegalArgumentException("Parameter:[numbers] cannot be null.");
        }

        return Arrays.stream(numbers).reduce(1, this::multiply);
    }

    @Override
    public float divide(float left, float right) {
        double result = (double) left / right;

        try {
            _helper.checkIfValid(result);
        }
        catch (ArithmeticException e) {
            throw new RuntimeException(e);
        }

        return (float) result;
    }

    @Override
    public float divideManyFrom(int left, int... dividingNums) {
        if (dividingNums == null) {
            throw new IllegalArgumentException("Parameter:[dividingNums] cannot be null.");
        }

        int dividingNumberResult = multiplyMany(dividingNums);
        return divide(left, dividingNumberResult);
    }
}

