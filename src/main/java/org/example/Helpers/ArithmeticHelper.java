package org.example.Helpers;

public class ArithmeticHelper {
    public void checkIfValid(Number number) {
        if (number instanceof Long) {
            long converted = (long) number;
            if (converted > Integer.MAX_VALUE || converted < Integer.MIN_VALUE) {
                throw new ArithmeticException("Number is not valid as it has been overflown the boundaries of integer.");
            }
        }
        else if (number instanceof Double) {
            double converted = (double) number;
            if (converted > Float.MAX_VALUE || converted < Float.MIN_VALUE) {
                throw new ArithmeticException("Number is not valid as it has been overflown the boundaries of float.");
            }
        }
    }

    public boolean tryCheckIfValid(Number number) {
        try {
            checkIfValid(number);
        }
        catch (ArithmeticException e) {
            return false;
        }
        return true;
    }
}
