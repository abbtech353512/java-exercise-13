package org.example.ServletServices;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.Helpers.ArithmeticHelper;
import org.example.ServiceImplementations.CoreCalculator;
import org.example.Services.CalculatorService;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CalculationAddServlet", urlPatterns = {"/calculator/add", "/calc/add"})
public class CalculatorAddServlet extends HttpServlet {
    CalculatorService calculator;
    @Override
    public void init() throws ServletException {
        calculator = new CoreCalculator(new ArithmeticHelper());
        System.out.println("Servlet initialized");
        super.init();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("On any method beginning i run initially.");
        super.service(req, resp);
    }

    @Override
    public void destroy() {
        System.out.println("I dispose the servlet");
        super.destroy();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String headerValue = req.getHeader("x-calculation");
        System.out.println("Header[x-calculation]: " + headerValue);

        int left = Integer.parseInt(req.getParameter("left"));
        int right = Integer.parseInt(req.getParameter("right"));

        PrintWriter writer = resp.getWriter();
        resp.setContentType("application/json");

        writer.write(String.format("""
                {
                    "result" : %s
                }
                """, calculator.add(left, right)));

        writer.close();
        System.out.println(req.getRequestURI());
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var reader = req.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
        reader.close();

        resp.setContentType("application/json");
        var writer = resp.getWriter();
        writer.write("""
                {
                    "result": "Success!"
                }
                """);

        writer.close();

        super.doPost(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        super.doDelete(req, resp);
    }
}
